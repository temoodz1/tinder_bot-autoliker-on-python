from selenium import webdriver
from time import sleep

class Tinder_bot:
    def __init__(self):
        self.driver = webdriver.Chrome()

    def log_in(self):
        self.driver.get('https://tinder.com/')
        sleep(5)
        fb_button = self.driver.find_element_by_xpath(
            '//*[@id="modal-manager"]/div/div/div[1]/div/div[3]/span/div[2]/button')
        fb_button.click()
        # to switch on pop-up window
        base_window = self.driver.window_handles[0]
        sleep(2)
        self.driver.switch_to.window(self.driver.window_handles[2])
        email_input = self.driver.find_element_by_xpath('//*[@id="email"]')
        email_input.send_keys("Enter you fb email or number here")
        password_input = self.driver.find_element_by_xpath('//*[@id="pass"]')
        password_input.send_keys("enter yor fb password here")
        loggIn_button = self.driver.find_element_by_xpath('//*[@id="loginbutton"]')
        loggIn_button.click()
        self.driver.switch_to.window(base_window)
        sleep(5)
        popup_1 = self.driver.find_element_by_xpath('//*[@id="modal-manager"]/div/div/div/div/div[3]/button[1]')
        popup_1.click()
        popup_2 = self.driver.find_element_by_xpath('//*[@id="modal-manager"]/div/div/div/div/div[3]/button[1]')
        popup_2.click()
        sleep(1)
        try:
            popup_3 = self.driver.find_element_by_xpath('//*[@id="modal-manager"]/div/div/div[1]/div[2]/button[2]')
            popup_3.click()
        except:
            pass

    def like(self):
        like_button = self.driver.find_element_by_xpath('//*[@id="content"]/div/div[1]/div/main/div[1]/div/div/div[1]/div/div[2]/div[4]/button')
        like_button.click()
    def auto_swipe(self):
        sleep(10)
        while True:
            try:
                sleep(0.1)
                self.like()
            except Exception:
                try:
                    self.add_popup_close()
                except:
                    try:
                        self.match_popup_close()
                    except:
                            self.outOfLikes_popup_close()

    def add_popup_close(self):
        popup=self.driver.find_element_by_xpath('//*[@id="modal-manager"]/div/div/div[2]/button[2]')
        popup.click()


    def match_popup_close(self):
        matchClose_button=self.driver.find_element_by_xpath('//*[@id="modal-manager-canvas"]/div/div/div[1]/div/div[3]/a')
        matchClose_button.click()
    def outOfLikes_popup_close(self):
        pass
        close=self.driver.find_element_by_xpath('//*[@id="modal-manager"]/div/div/div[3]/button[2]')
        close.click()

def startup():
    bot = Tinder_bot()
    bot.log_in()
    bot.auto_swipe()
startup()
